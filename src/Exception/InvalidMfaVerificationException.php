<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Exception;

final class InvalidMfaVerificationException extends \Exception
{
    public static function missingRequestParameter(): self
    {
        return new self('The request is missing the required `otp` value.');
    }

    public static function make(): self
    {
        return new self('The provided verification data did not match the configured one.');
    }
}
