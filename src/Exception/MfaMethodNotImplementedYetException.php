<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Exception;

final class MfaMethodNotImplementedYetException extends \Exception
{
    public static function make(): self
    {
        return new self('MFA method not implemented yet.');
    }
}
