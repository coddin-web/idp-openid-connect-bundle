<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\OpenIDConnect;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

final class ForcedRedirectDeterminator
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    public function execute(Request $request): bool
    {
        $baseUrl = $request->getBaseUrl();
        $possibleForcedRedirectPaths = \explode(
            ',',
            /** @phpstan-ignore-next-line */
            $this->parameterBag->get('coddin_identity_provider.auth.forced_redirect_allowed_paths'),
        );

        foreach ($possibleForcedRedirectPaths as $possibleForcedRedirectPath) {
            if (\str_contains($baseUrl, $possibleForcedRedirectPath)) {
                return true;
            }
        }

        return false;
    }
}
