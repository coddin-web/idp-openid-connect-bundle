<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\View\MultiFactorAuthentication;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Coddin\IdentityProvider\Entity\OpenIDConnect\Enum\MfaMethod;
use Coddin\IdentityProvider\Entity\OpenIDConnect\User;
use Coddin\IdentityProvider\Exception\MfaMethodNotImplementedYetException;
use Coddin\IdentityProvider\Service\Auth\MfaProvider;
use OTPHP\TOTP;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

final class Renderer
{
    public function __construct(
        private readonly Environment $twig,
        private readonly MfaProvider $mfaProvider,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * @throws MfaMethodNotImplementedYetException
     */
    public function determineView(
        User $user,
        MfaMethod $mfaMethod,
    ): Response {
        $mfaMethodConfigKey = match ($mfaMethod) {
            MfaMethod::METHOD_AUTHENTICATOR_APP, MfaMethod::METHOD_SMS => 'totp_secret_key',
            default => throw MfaMethodNotImplementedYetException::make(),
        };

        // Todo: This is only for OTP based MFA's, determine this based on the mfaMethodType.
        $oneTimePassword = TOTP::create();
        $secret = $oneTimePassword->getSecret();

        $this->mfaProvider->mfaMethodRegistration(
            $user,
            MfaMethod::METHOD_AUTHENTICATOR_APP,
            [
                $mfaMethodConfigKey => $secret,
            ],
        );

        // Todo: This is Authenticator App specific, move this logic to a Service and determine if this even needs to happen.
        $qrRendered = new ImageRenderer(
            rendererStyle: new RendererStyle(400),
            imageBackEnd: new SvgImageBackEnd(),
        );
        $writer = new Writer($qrRendered);
        $qrCodeData = \sprintf(
            'otpauth://totp/%s?secret=%s&issuer=%s',
            $user->getUsername(),
            $secret,
            /* @phpstan-ignore-next-line */
            $this->parameterBag->get('idp.company_name'),
        );
        $qrCodeRaw = $writer->writeString($qrCodeData);

        $content = $this->twig->render(
            name: '@CoddinIdentityProvider/account/security/setup_mfa.html.twig',
            context: [
                'qrCodeData' => $qrCodeRaw,
            ],
        );

        $response = new Response();
        $response->setContent($content);

        return $response;
    }
}
