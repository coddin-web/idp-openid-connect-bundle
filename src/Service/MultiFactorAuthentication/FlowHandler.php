<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\MultiFactorAuthentication;

use Coddin\IdentityProvider\Entity\OpenIDConnect\Enum\MfaType;
use Coddin\IdentityProvider\Entity\OpenIDConnect\User;
use Coddin\IdentityProvider\Entity\OpenIDConnect\UserMfaMethod;
use Coddin\IdentityProvider\Exception\InvalidMfaVerificationException;
use Coddin\IdentityProvider\Exception\UserMfaMethodNotFoundException;
use Coddin\IdentityProvider\Repository\UserMfaMethodRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

final class FlowHandler
{
    public function __construct(
        private readonly UserMfaMethodRepository $userMfaMethodRepository,
        private readonly MethodHandlerDeterminator $methodHandlerDeterminator,
    ) {
    }

    /**
     * @throws UserMfaMethodNotFoundException
     */
    public function sendOneTimePasswordToUser(
        User $user,
    ): void {
        $userMfaMethod = $this->userMfaMethodRepository->getActiveMfaMethodForUser($user);
        $mfaMethodHandler = $this->methodHandlerDeterminator->execute($userMfaMethod);

        $mfaMethodHandler->sendOtp($userMfaMethod);
    }

    /**
     * @throws InvalidMfaVerificationException
     * @throws UserMfaMethodNotFoundException
     */
    public function processSubmittedMfa(
        Request $request,
        Security $security,
    ): void {
        $user = $security->getUser();
        if (!$user instanceof User) {
            throw new \LogicException('Incorrect User type provided');
        }

        $userMfaMethod = $this->userMfaMethodRepository->getActiveMfaMethodForUser($user);

        switch ($userMfaMethod->getMfaMethod()->getType()) {
            case MfaType::TYPE_TOTP->value:
                $this->handleTotp($request, $userMfaMethod);
                break;
            case MfaType::TYPE_U2F->value:
                $this->handleU2f($request, $userMfaMethod);
                break;
            default:
                throw new \LogicException(
                    sprintf(
                        'Unsupported MFA type `%s`',
                        $userMfaMethod->getMfaMethod()->getType(),
                    ),
                );
        }
    }

    /**
     * @throws InvalidMfaVerificationException
     */
    private function handleTotp(
        Request $request,
        UserMfaMethod $userMfaMethod,
    ): void {
        // Todo: Better validation.
        if (!$request->request->has('otp')) {
            throw InvalidMfaVerificationException::missingRequestParameter();
        }

        $submittedOtp = $request->request->get('otp');

        $mfaMethodHandler = $this->methodHandlerDeterminator->execute($userMfaMethod);

        // TODO: Not use an unstructured array.
        $verificationData = [];
        // TODO: Not use string as key.
        $verificationData['otp'] = $submittedOtp;

        if ($mfaMethodHandler->verifyAuthentication($userMfaMethod, $verificationData) === false) {
            throw InvalidMfaVerificationException::make();
        }
    }

    private function handleU2f(
        Request $request,
        UserMfaMethod $userMfaMethod,
    ): void {
        throw new \LogicException('This type of MFA is not implemented yet');
    }
}
