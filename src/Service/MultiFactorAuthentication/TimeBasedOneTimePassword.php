<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\MultiFactorAuthentication;

final class TimeBasedOneTimePassword
{
    public static function generateSecret(int $length = 32): string
    {
        $b32 = "234567QWERTYUIOPASDFGHJKLZXCVBNM";
        $s = "";

        for ($i = 0; $i < $length; $i++) {
            $s .= $b32[rand(0, 31)];
        }

        return $s;
    }
}
