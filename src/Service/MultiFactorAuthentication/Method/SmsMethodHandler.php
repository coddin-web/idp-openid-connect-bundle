<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\MultiFactorAuthentication\Method;

use Coddin\IdentityProvider\Entity\OpenIDConnect\UserMfaMethod;
use Coddin\IdentityProvider\Entity\OpenIDConnect\UserMfaMethodConfig;
use Coddin\IdentityProvider\Repository\UserMfaMethodConfigRepository;
use Coddin\IdentityProvider\Service\MultiFactorAuthentication\Method\Client\SmsClientInterface;
use Coddin\IdentityProvider\Service\MultiFactorAuthentication\TimeBasedOneTimePassword;
use Coddin\IdentityProvider\Service\MultiFactorAuthentication\TimeBasedOneTimePasswordVerification;
use OTPHP\TOTP;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class SmsMethodHandler implements MfaMethodHandler
{
    // TODO: Make configurable.
    private const TOTP_PERIOD = (5 * 60);
    // TODO: move to enum.
    public const CONFIG_KEY = 'totp_secret_key';

    public function __construct(
        private readonly UserMfaMethodConfigRepository $userMfaMethodConfigRepository,
        private readonly TimeBasedOneTimePasswordVerification $timeBasedOneTimePasswordVerification,
        private readonly SmsClientInterface $smsClient,
        private readonly ParameterBagInterface $parameterBag,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function handleRegistrationRequest(UserMfaMethod $userMfaMethod): void
    {
        $secretKey = TimeBasedOneTimePassword::generateSecret();
        $this->userMfaMethodConfigRepository->updateValueForUserWithKey(
            $userMfaMethod,
            self::CONFIG_KEY,
            $secretKey,
        );

        $this->sendOtp($userMfaMethod);
    }

    /**
     * @inheritDoc
     */
    public function verifyAuthentication(
        UserMfaMethod $userMfaMethod,
        array $verificationData,
    ): bool {
        // TODO: Remove inner-workings-knowledge ('period' array key) needed for this to work.
        $verificationData['period'] = self::TOTP_PERIOD;

        return $this->timeBasedOneTimePasswordVerification->verifyAuthentication(
            userMfaMethod: $userMfaMethod,
            verificationData: $verificationData,
        );
    }

    public function sendOtp(UserMfaMethod $userMfaMethod): void
    {
        $userMfaMethodConfigs = $userMfaMethod->getUserMfaMethodConfigs();

        // Todo: Move key/value logic to a service/helper that handles getting the correct value.
        /** @var UserMfaMethodConfig $userMfaMethodPhoneNumber */
        $userMfaMethodPhoneNumber = $userMfaMethodConfigs->filter(
            fn(UserMfaMethodConfig $userMfaMethodConfig) => $userMfaMethodConfig->getKey() === 'phone_number',
        )->first();
        /** @var UserMfaMethodConfig $userMfaMethodSecret */
        $userMfaMethodSecret = $userMfaMethodConfigs->filter(
            fn(UserMfaMethodConfig $userMfaMethodConfig) => $userMfaMethodConfig->getKey() === 'totp_secret_key',
        )->first();

        $oneTimePassword = TOTP::create(
            secret: $userMfaMethodSecret->getValue(),
            period: self::TOTP_PERIOD,
        );

        $this->smsClient->sendTextMessage(
            /* @phpstan-ignore-next-line */
            originator: $this->parameterBag->get('idp.company_name'),
            recipient: $userMfaMethodPhoneNumber->getValue(),
            // Todo? Better / bigger message? Translatable?
            body:  $this->translator->trans(id: 'account.mfa.your_sms_verification_code', parameters:  ['%otp%' => $oneTimePassword->now()]),
        );
    }
}
