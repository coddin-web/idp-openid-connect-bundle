<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\MultiFactorAuthentication\Method;

use Coddin\IdentityProvider\Entity\OpenIDConnect\UserMfaMethod;
use Coddin\IdentityProvider\Repository\UserMfaMethodConfigRepository;
use Coddin\IdentityProvider\Service\MultiFactorAuthentication\TimeBasedOneTimePassword;
use Coddin\IdentityProvider\Service\MultiFactorAuthentication\TimeBasedOneTimePasswordVerification;

final class AuthenticatorAppMethodHandler implements MfaMethodHandler
{
    // TODO: move to enum.
    public const CONFIG_KEY = 'totp_secret_key';

    public function __construct(
        private readonly UserMfaMethod $userMfaMethod,
        private readonly UserMfaMethodConfigRepository $userMfaMethodConfigRepository,
        private readonly TimeBasedOneTimePasswordVerification $timeBasedOneTimePasswordVerification,
    ) {
    }

    public function handleRegistrationRequest(UserMfaMethod $userMfaMethod): void
    {
        $secretKey = TimeBasedOneTimePassword::generateSecret();
        $this->userMfaMethodConfigRepository->updateValueForUserWithKey(
            $userMfaMethod,
            self::CONFIG_KEY,
            $secretKey,
        );
    }

    /**
     * @inheritDoc
     */
    public function verifyAuthentication(
        UserMfaMethod $userMfaMethod,
        array $verificationData
    ): bool {
        return $this->timeBasedOneTimePasswordVerification->verifyAuthentication(
            userMfaMethod: $this->userMfaMethod,
            verificationData: $verificationData,
        );
    }

    public function sendOtp(UserMfaMethod $userMfaMethod): void
    {
        // Do nothing.
    }
}
