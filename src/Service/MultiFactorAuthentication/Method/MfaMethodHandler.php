<?php

declare(strict_types=1);

namespace Coddin\IdentityProvider\Service\MultiFactorAuthentication\Method;

use Coddin\IdentityProvider\Entity\OpenIDConnect\UserMfaMethod;

interface MfaMethodHandler
{
    public function handleRegistrationRequest(UserMfaMethod $userMfaMethod): void;

    /**
     * @param array<string, mixed> $verificationData
     */
    public function verifyAuthentication(
        UserMfaMethod $userMfaMethod,
        array $verificationData,
    ): bool;
}
